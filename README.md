# shopTNT开源商城系统-前端（部署脚本-配置文件）

## 交流、反馈

### 推荐
欢迎大家加群：

官方QQ群：<a target="_blank" href="https://qm.qq.com/cgi-bin/qm/qr?k=RtRIL7WomrO79uDDDp4HihX_GH1xMIPD&jump_from=webapi"><img border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt="ShopTNT开源交流群" title="ShopTNT开源交流群"></a> 766503360

大家可以在群中多多交流需求，业务，技术等问题，我们也会关注到。也会尽量满足大家的需求，定期更新新的功能。


## 部署文档
文档地址：https://docs.shoptnt.cn/docs/5.2.3

## 项目开源地址
shopTNT开源商城系统：https://gitee.com/bbc-se


## 使用须知

1. 允许个人学习使用。
2. 允许用于学习、毕业设计等。
3. 禁止将本开源的代码和资源进行任何形式任何名义的出售。
4. 限制商业使用，如需[商业使用](http://www.shoptnt.cn)联系QQ：2025555598 或微信扫一扫加我微信。

![](https://shoptnt-bbc.oss-cn-beijing.aliyuncs.com/tnt/12261709021247_.pic.jpg?x-oss-process=style/300x300)


